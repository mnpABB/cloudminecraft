<?php
namespace Logger;

use pocketmine\Server;

use pocketmine\event\Listener;
use pocketmine\event\player\PlayerJoinEvent;
use pocketmine\event\player\PlayerQuitEvent;

class Logger implements Listener{
  public $playerLogger;

  function __construct(){
    $this->playerLogger = new PlayerLogger();
    $this->playerLogger->add(Server::getInstance()->getOnlinePlayers());
  }

  public function loggingPlayer_join(PlayerJoinEvent $event){
    $this->playerLogger->add(count($this->getServer()->getOnlinePlayers()));
  }

  public function loggingPlayer_exit(PlayerQuitEvent $event){
    $this->playerLogger->add(count($this->getServer()->getOnlinePlayers()));
  }

}
 ?>
