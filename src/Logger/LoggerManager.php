<?php
namespace Logger;

class LoggerManager{
  private static $_instance;
  private $configs;
  public function __construct(){
    $this->configs = [];
  }
  public static function getInstance()
  {
      if (!isset(self::$_instance)) {
          self::$_instance = new LoggerManager();
      }

      return self::$_instance;
  }

  public function closeConfigs(){
    foreach($this->configs as $config){
      $config->disable();
    }
  }

  public function registConfig($config){
    $this->configs[] = $config;
  }

}
 ?>
