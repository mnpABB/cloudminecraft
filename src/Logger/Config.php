<?php

namespace Logger;

use Logger\LoggerManager;

class Config{
  public $tmp;
  public $enable;
  public $buf;

  function __construct()
  {
    $this->tmp = tmpfile();
    $this->enable = TRUE;
    $this->buf = [];
    LoggerManager::getInstance()->registConfig($this);
  }

  function disable(){
    fclose($this->tmp);
    $this->enable = FALSE;
  }

  function  save($value){
    if($this->enable){
      fseek($this->tmp, 0);
      ftruncate($this->tmp, 0);

      $write_str = "";
      foreach($value as $key => $val){
        $write_str = $write_str . $key . '  ' . $val . PHP_EOL;
      }
    //  echo $write_str;
      fwrite($this->tmp, $write_str);
      return TRUE;
    }else{
      return FALSE;
    }
  }

  function convert($text){
    $lines = preg_split('(\r\n|\r|\n)s', $text);
    $res = [];
    foreach($lines as $line){
      $buf = preg_split('(  )', $line);
      if(isset($buf[1])){
        $res[$buf[0]] = $buf[1];
      }
    }
    return $res;
  }

  function  read($fileOnly=FALSE){
    if($this->enable){
      fseek($this->tmp, 0);
      $size = fstat($this->tmp)['size'];
      if($size > 0){
        $res = $this->convert(fread($this->tmp, $size));
        if($fileOnly){
          return $res;
        }else{
          return array_merge($res, $this->buf);
        }
      }else{
        return $this->buf;
      }
    }else{
      return FALSE;
    }
  }

  function getLast($val){
    return end($val);
  }

  function  add($val){
    if($this->enable){
      if($val != $this->getLast($this->buf)){
        $this->buf[microtime()] = $val;
      }

      if(count($this->buf) > 1000){
        $ohshit = $this->read(TRUE);
        $this->save(array_merge($this->buf, $ohshit));
        $this->buf = [];
        return TRUE;
      }
    }else{
      return FALSE;
    }
  }
}


 ?>
