<?php
namespace main;
use pocketmine\Server;
use pocketmine\utils\Config;
use pocketmine\plugin\PluginBase;

// Server
use Server\HTTPServer;
use Server\HTTPResponse;
use Server\HTTPRequest;
use pocketmine\scheduler\CallbackTask;

// API
use pocketmine\Player;
use pocketmine\event\TranslationContainer;

// API Events
use Logger\Logger;
use Logger\LoggerManager;
use Reaction\ResponseManager;

class main extends PluginBase{
  // Log things..
  public $log_tick_overload; // array( EPOC => Tick Avarage)
  public $log_packet;
  public $log_players;// array( EPOC => Player counts)
  public $log_load;


  public $reactions;

  public function onDisable(){

    // Stop server..
    $this->getLogger()->info("Stop the http server...");
    $this->serverThread->synchronized(function (HTTPServer $thread) {
        $thread->running = false;
    }, $this->serverThread);
    try {
      $fp = fsockopen("localhost", 55555);
    }catch(Exception $e){

    }
    $this->serverThread->join();
    LoggerManager::getInstance()->closeConfigs();
  }

  function get_first_key($array)
  {
    reset($array);
    return key($array);
  }

  function array_last(array $array)
  {
    return end($array);
  }

  public function onEnable(){
    $this->responseManager = new ResponseManager();
    $this->reactions = [];

    $this->log_players = [];
    $this->log_players[time()] = count($this->getServer()->getOnlinePlayers());

    $log_tick_overload = [];

    $this->getServer()->getPluginManager()->registerEvents(new Logger($this), $this);
    $this->getServer()->getScheduler()->scheduleRepeatingTask(new CallbackTask([$this,"ticker"]),1);


    $this->saveDefaultConfig();//resourcesにあるconfig.ymlファイルをデータフォルダに入れて保存
    $this->reloadConfig();//作成されたファイルを再読み込み
    if(!file_exists($this->getDataFolder())){//configファイルを入れるフォルダがあるかを確認
        @mkdir($this->getDataFolder(), 0744, true);//なければフォルダを作成
    }
    //$this->config変数にConfigオブジェクトを入れておく
    $this->config = new Config($this->getDataFolder() . "config.yml", Config::YAML);

    $this->users = $this->config->get('users');

    $this->reactions['/api/tick_average/log'] = function($request){
      $result = array(
        'code' => 0,
        'message' => "success",
        'path' => $request->uri,
        'result' => $this->log_tick_overload
      );
      $response = new HTTPResponse(200, json_encode($result),null,null);
      $response->headers['Content-Type'] = "text/javascript; charset=utf-8";
      return $response;
    };

    $this->reactions['/api/pHTTP_METHOD_NOT_ALLOWEDlayers/log'] = function($request){
      $b = $this->log_players;
      $b[time()] = $this->array_last($b);
      $result = array(
        'code' => 0,
        'message' => "success",
        'path' => $request->uri,
        'result' => $b
      );
      $response = new HTTPResponse(200, json_encode($result),null,null);
      $response->headers['Content-Type'] = "text/javascript; charset=utf-8";
      return $response;
    };

    $this->reactions['/api/network/log'] = function($request){
      $result = array(
        'code' => 0,
        'message' => "success",
        'path' => $request->uri,
        'result' =>$this->log_packet
      );
      $response = new HTTPResponse(200, json_encode($result),null,null);
      $response->headers['Content-Type'] = "text/javascript; charset=utf-8";
      return $response;
    };

    $this->reactions['/api/load/log'] = function($request){
      $result = array(
        'code' => 0,
        'message' => "success",
        'path' => $request->uri,
        'result' =>$this->log_load
      );
      $response = new HTTPResponse(200, json_encode($result),null,null);
      $response->headers['Content-Type'] = "text/javascript; charset=utf-8";
      return $response;
    };

    $g = new HTTPServer($this->getLogger());
    $g->start();
    $this->serverThread = $g;

    return;

  }

  public function loggingPlayer_join(PlayerJoinEvent $event){
    $this->log_players[time()] = count($this->getServer()->getOnlinePlayers());
  }

  public function loggingPlayer_exit(PlayerQuitEvent $event){
    $this->log_players[time()] = count($this->getServer()->getOnlinePlayers());
  }

  public function ticker(){
    $queue = unserialize($this->serverThread->queue);
    if($queue instanceof HTTPRequest){
      $req = $queue;
      //echo var_dump($ret);
      $this->serverThread->queue = serialize($this->responseManager->act($this, $req));
      $this->serverThread->needing = false;
    }
    if(($this->getServer()->getTick() % 10) === 0){
      $network = $this->getServer()->getNetwork();
      $this->log_packet[time()] = array(round($network->getUpload() / 1024, 2), round($network->getDownload() / 1024, 2));
      if($this->get_first_key($this->log_packet) < (time() - 86400)){
        array_shift($this->log_packet);
      }

      $this->log_load[time()] = $this->getServer()->getTickUsage();
      if($this->get_first_key($this->log_packet) < (time() - 86400)){
        array_shift($this->log_packet);
      }
    }
    // Loging tick average 100-ticks intervals.
    if(($this->getServer()->getTick() % 100) === 0){

      $this->log_tick_overload[time()] = $this->getServer()->getTicksPerSecondAverage();

      if(count($this->log_tick_overload) > 86400){
        array_shift($this->log_tick_overload);
      }

      // Get first key
      if($this->get_first_key($this->log_players) < (time() - 86400)){
        array_shift($this->log_players);
      }

    }
  }
}
