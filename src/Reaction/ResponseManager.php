<?php
namespace Reaction;

use Reaction\Responses\Error\NotFound;

use Reaction\Responses\Server\GetBannedPlayers;
<<<<<<< HEAD
use Reaction\Responses\Server\GetOnlinePlayers;
use Reaction\Responses\Server\GetPlayerSkin;
use Reaction\Responses\Server\GetServerStatus;


use Reaction\Responses\API\APIHello;
use Reaction\Responses\LoginSysten\Login;
use Reaction\Responses\API\Log\ServerLoadLogger;
use Reaction\Responses\API\Log\ServerLagLogger;
use Reaction\Responses\API\Log\ServerUploadLogger;
use Reaction\Responses\API\Log\ServerDownloadLogger;
=======

use Reaction\Responses\API\APIHello;
use Reaction\Responses\API\Log\ServerLoadLogger;
>>>>>>> 6fa365657e776c8f76011b7d83f2ef89c8f3e5f6

use Reaction\Responses\Util;


class ResponseManager{
  private $acts;


  public function __construct(){
    $acts = [];


    $this->addReaction('/api/hello', new APIHello());
    $this->addReaction('/server/banned', new GetBannedPlayers());
<<<<<<< HEAD
    $this->addReaction('/server/online', new GetOnlinePlayers());
    $this->addReaction('/server/skin',   new GetPlayerSkin());
    $this->addReaction('/server/status', new GetServerStatus());
    $this->addReaction('/api/log/load',  new ServerLoadLogger());
    $this->addReaction('/api/log/lag',   new ServerLagLogger());
    $this->addReaction('/api/log/upload',new ServerUploadLogger());
    $this->addReaction('/api/log/download', new ServerDownloadLogger());

=======
    $this->addReaction('/api/log/load', new ServerLoadLogger());
>>>>>>> 6fa365657e776c8f76011b7d83f2ef89c8f3e5f6
    $this->addReaction(-1, new NotFound());
  }

  public function act($main, $request){

    if(array_key_exists($request->uri, $this->acts)){
      if($request->method == Util::HTTP_GET){
        $response = $this->acts[$request->uri]->get($main, $request);
      }else{
        $response = $this->acts[$request->uri]->post($main, $request);
      }
    }else{
      $response = $this->acts[-1]->get($main, $request);
    }
    $time = strftime("%H:%M:%S");

    $main->getLogger()->info("{$request->remote_addr} - - [$time] \"{$request->request_line}\" {$response->status} {$response->bytes_written}");
    return $response;
  }

  private function addReaction($path, $class){
    $this->acts[$path] = $class;
  }
}

?>
