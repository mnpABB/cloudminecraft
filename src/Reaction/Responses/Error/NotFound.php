<?php
/*
* [Name] NotFound
* [Path] /*
* [Description] NotFound
*/


namespace Reaction\Responses\Error;

use Reaction\APIInterface;
use Server\HTTPResponse;

use Reaction\Responses\Util;

class NotFound implements APIInterface{

  public function get($main, $request){
    return Util::return_body_error($request, Util::HTTP_NOT_FOUND, 'Not found..');;
  }

  public function post($main, $request){
    return Util::return_body_error($request, Util::HTTP_NOT_FOUND, 'Not found..');
  }
}

 ?>
