<?php
/*
* [Name] Loggin
* [Path] /api/session/login
* [Description] Login!
* [method] POST
* [Params] UserID, UserPassword ( show config. )
*/


namespace Reaction\Responses\API\LoginSystem;

use Reaction\Responses\Util;
use Reaction\APIInterface;

use Reaction\Responses\Session\Session;
use Reaction\Responses\Session\SessionManager;

class Login implements APIInterface{
<<<<<<< HEAD

=======
>>>>>>> 6fa365657e776c8f76011b7d83f2ef89c8f3e5f6
  public function get($main, $request){
    return Util::return_body_error($request, Util::HTTP_METHOD_NOT_ALLOWED, 'request is POST');
  }

  public function post($main, $request){
    if(!$request->body){
      return Util::return_body_error($response, Util::HTTP_INTERNAL_SERVER_ERROR, 'require body');
    }
    $body = json_decode($request->body);

    if (array_key_exists($main->users, $body['name'])){
      if($main->users[$body['name']] == $body['password']){
        $user = new Session($body['name']);
        SessionManager::getInstance()->registUser($user);
        $res = array(
          'id' => $user->getSession(),
          'name' => $user->getName(),
          'time' => $user->getTime(),
          'enable' => $user->isEnable()
        );
        return Util::return_body_json($request, $res);
      }
    }
  }
}
