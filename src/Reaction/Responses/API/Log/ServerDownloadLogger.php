<?php
/*
* [Name] ServerDownloadLogger
* [Path] /api/log/download
* [Description] Logging download.
* [method] GET
*/


namespace Reaction\Responses\API\Log;

use Reaction\Responses\Util;
use Reaction\APIInterface;

use Logger\Config;

use pocketmine\Server;
use pocketmine\scheduler\CallbackTask;

class ServerDownloadLogger implements APIInterface{

  public function __construct(){
    $this->config = new Config();
    Server::getInstance()->getScheduler()->scheduleRepeatingTask(new CallbackTask([$this,"ticker"]),1);
  }

  public function get($main, $request){
    return Util::return_body_json($request, $this->config->read());
  }

  public function post($main, $request){
    return Util::return_body_error($request, Util::HTTP_METHOD_NOT_ALLOWED, 'request is GET');
  }

  public function ticker(){
    if(Server::getInstance()->getTicksPerSecondAverage() < 12){
      $this->config->add(round(Server::getInstance()->getNetwork()->getDownload() / 1024, 2));
    }
  }
}
