<?php
/*
* This class is test of server.
* [Name] Hello
* [Path] /api/hello
* [Description] print only hello
*/


namespace Reaction\Responses\API;

use Reaction\APIInterface;
use Server\HTTPResponse;

class APIHello implements APIInterface{
  public function get($main, $request){
    $result = array(
      'code' => 0,
      'message' => "success",
      'path' => $request->uri,
      'result' => array()
    );
    $response = new HTTPResponse(200, json_encode($result),null,null);
    $response->headers['Content-Type'] = "text/javascript; charset=utf-8";
    return $response;
  }

  public function post($main, $request){
    $result = array(
      'code' => 0,
      'message' => "success",
      'path' => $request->uri,
      'result' => array()
    );
    $response = new HTTPResponse(200, json_encode($result),null,null);
    $response->headers['Content-Type'] = "text/javascript; charset=utf-8";
    return $response;
  }
}

 ?>
