<?php
/*
* Warning: This class is not response.
*/
namespace Reaction\Responses\Session;

use Reaction\Responses\Session\Session;

class SessionManager{
  private $users;

  private static $_INSTANCE;

  public function registUser($user){
    if(!$user instanceof Session){
      return FALSE;
    }else{
      $cnt = 0;
      foreach($this->users as $registedUser){
        if($registedUser->getName() == $user->getName()){
          unset($this->users[$cnt]);
        }
        $cnt++;
      }
      array_push($this->users, $user);
    }
  }

  public function isEnable($user){
    if(!$user instanceof Session){
      return FALSE;
    }else{
      $cnt = 0;
      foreach($this->users as $registedUser){
        if($registedUser->getName() == $user->getName()){
          if((time() - $registedUser->getCreated()) > 86400){
            return FALSE;
          }
          return TRUE;
        }
        $cnt++;
      }
    }
  }

  public static function getInstance(){
    if(!isset(self::$_INSTANCE)){
      self::$_INSTANCE = new SessionManager;
    }
    return self::$_INSTANCE;
  }
}
