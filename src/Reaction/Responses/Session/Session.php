<?php
/*
* Warning: This class is not response.
*/
namespace Reaction\Responses\Session;

class Session{
  private $session_id;
  private $enable;
  private $timestamp;
  private $name;

  public function __construct($name){
    $this->session_id = uniqid('SESSIONID:');
    $this->enable = TRUE;
    $this->timestamp = time();
    $this->name = $name;
  }

  public function isEnable(){
    return $this->enable;
  }

  public function getCreated(){
    return $this->timestamp;
  }

  public function check($id){
    return $id == $this->session_id;
  }

  public function getName(){
    return $this->name;
  }

  public function getSession(){
    return $this->session_id;
  }
}
