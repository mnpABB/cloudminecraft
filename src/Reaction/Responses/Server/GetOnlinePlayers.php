<?php
/*
* [Name] GetPlayers
* [Path] /server/players
* [Description] Get skin
*/


namespace Reaction\Responses\Server;

use Reaction\APIInterface;
use Reaction\Responses\Util;

use Server\HTTPResponse;

class GetOnlinePlayers implements APIInterface{
  public $path = '/server/online';

  public function get($main, $request){
    $players = $this->getServer()->getOnlinePlayers();
    $result = [];
    foreach($players as $player){
        array_push($result, Util::playerToJson($player));
    }
    return Util::return_body_json($result);
  }

  public function post($main, $request){
    return Util::return_body_error($request, Util::HTTP_METHOD_NOT_ALLOWED, 'Request is get');
  }
}
