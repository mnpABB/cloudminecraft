<?php
/*
* [Name] GetSkin
* [Path] /server/skin
* [Description] Get skin
* [Method] GET
* [Param] clientid
*/


namespace Reaction\Responses\Server;

use Reaction\APIInterface;
use Reaction\Responses\Util;

use Server\HTTPResponse;

class GetPlayerSkin implements APIInterface{
  public $path = '/server/skin';

  private function str_to_head($data){
    // Pasted from http://pastebin.com/Dq4kp5aN
    // Thank you so so much, Samueljh1H8sLag.
    //echo $data;
    $stro = chunk_split($data, 2, " ");
    $str = explode(" ", $stro);
    $img = imagecreatetruecolor(8,8);
    $cl = explode(" ", chunk_split($data, 8, " "));
    $colors = [];
    foreach($cl as $group){
    $d = explode(" ", chunk_split($group, 2, " "));
    array_pop($d);
    if(count($d) === 4) {
        if(!isset($colors[$d[0].$d[1].$d[2]])) $colors[$d[0].$d[1].$d[2]] = imagecolorallocate($img, hexdec($d[0]), hexdec($d[1]), hexdec($d[2]));
      }
    }
    $dat = explode(" ", chunk_split($data, 8, " "));
    $x = 0;
    $y = 0;
    $c = 0;
    $black = imagecolorallocate($img, 0, 0, 0);
    while ($y <= 32) {
      $dt = explode(" ", chunk_split($dat[$c], 2, " "));
      array_pop($dt);
        if($y >= 8 && $x >= 8 && $x <= 16 && $y <= 16){
        imagesetpixel($img,$x - 8,$y - 8, $colors[$dt[0].$dt[1].$dt[2]]);
      }
      if ($x === 64) {
        $x = 0;
        $y++;
      }
      $x++;
      $c++;
    }
    ob_start();
    imagepng($img, null, 9); // png画像をminify
    return ob_get_clean();

    return $image_string;
  }

  public function get($main, $request){
    $players = $main->getServer()->getOnlinePlayers();
    if(!isset($request->query_string)){
      return Util::return_body_error($request, Util::HTTP_INTERNAL_SERVER_ERROR, 'require client id');
    }
    parse_str($request->query_string);
    if(!isset($clientid)){
      return Util::return_body_error($request, Util::HTTP_INTERNAL_SERVER_ERROR, 'require client id');
    }

    foreach($players as $player){
      if($player->getClientId() == $clientid){
        $response = new HTTPResponse(200, $this->str_to_head(bin2hex($player->getSkinData())),null,null);
        $response->headers['Content-Type'] = "image/png";
        return $response;
      }
    }
    return Util::return_body_error($request, Util::HTTP_INTERNAL_SERVER_ERROR, 'generating image error..');
  }

  public function post($main, $request){
    return Util::return_body_error($request, Util::HTTP_METHOD_NOT_ALLOWED, 'request must be get.');
  }
}

 ?>
