<?php
/*
* [Name] GetServerStatus
* [Path] /server/status
* [Description] Get server status
*/


namespace Reaction\Responses\Server;

use Reaction\APIInterface;
use Reaction\Responses\Util;

use Server\HTTPResponse;

use pocketmine\Server;

class GetServerStatus implements APIInterface{
  public $path = '/server/status';

  public function get($main, $request){
      $white = [];
      foreach(Server::getInstance()->getWhitelisted()->getAll() as $player => $val){
        array_push($white, $player);
      }
      $commands = [];
      foreach (Server::getInstance()->getCommandMap()->getCommands() as $command) {
        array_push($commands, array(
          'name' => $command->getName(),
          'permission' => $command->getPermission(),
          'label' => $command->getLabel(),
          'aliases' => $command->getAliases(),
          'permission_msg' => $command->getPermissionMessage(),
          'description' => Server::getInstance()->getLanguage()->translateString($command->getDescription()),
          'usage' => Server::getInstance()->getLanguage()->translateString($command->getUsage())
        ));
      }
      $result = array(
        'gamemode' => Server::getInstance()->getDefaultGamemode(),
        'gamemode_str' => Server::getInstance()->getLanguage()->translateString(Server::getInstance()->getGamemodeString(Server::getInstance()->getDefaultGamemode())),
        'is_hardcore' => Server::getInstance()->isHardcore(),
        'allow_flight' => Server::getInstance()->getAllowFlight(),
        'difficulty' => Server::getInstance()->getDifficulty(),
        'whitelist' => $white,
        'tick' => Server::getInstance()->getTick(),
        'tick_sec' => Server::getInstance()->getTicksPerSecond(),
        'tick_sec_average' => Server::getInstance()->getTicksPerSecondAverage(),
        'tick_usage' => Server::getInstance()->getTickUsage(),
        'commands' => $commands,
        'max_players' => Server::getInstance()->getMaxPlayers(),
        'server_name' => Server::getInstance()->getName()
      );
      return Util::return_body_json($request, $result);
  }

  public function post($main, $request){
    return Util::return_body_error($request, Util::HTTP_METHOD_NOT_ALLOWED, 'request is get');
  }
}
