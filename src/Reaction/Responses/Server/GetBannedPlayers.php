<?php
/*
* [Name] BannedPlayers
* [Path] /server/banned
* [Description] Show banned players
*/


namespace Reaction\Responses\Server;

use Reaction\Responses\Util;
use Reaction\APIInterface;

class GetBannedPlayers implements APIInterface{
  public $path = '/server/banned';

  public function get($main, $request){
    $name_banned_players = $main->getServer()->getNameBans()->getEntries();
    $ip_banned_players = $main->getServer()->getIpBans()->getEntries();
    $players = [];
    foreach($name_banned_players as $id => $value){
      $result['player_name'] = $id;
      $result['reason']      = $value->getReason();
      $result['source']      = $value->getSource();
      $result['time']        = $value->getCreated()->format("Y-m-d H:i:s O");
      array_push($players, $result);
    }
    return Util::return_body_json($request, $players);
  }

  public function post($main, $request){
    return Util::return_body_error($request, Util::HTTP_METHOD_NOT_ALLOWED, 'request is GET');
  }
}

 ?>
