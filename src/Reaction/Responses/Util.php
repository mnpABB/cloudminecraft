<?php
namespace Reaction\Responses;

use Server\HTTPResponse;

class Util{
  //// JSON Util
  public static function playerToJson(Player $player){
    if($player instanceof Player){
      return array(
        'player_name' => $player->getName(),
        'ip_addr' => $player->getAddress(),
        'gamemode' => $player->getGamemode(),
        'get_world_level' => $player->getLevel()->getFolderName(),
        'location' => array(
          'x' => $player->x,
          'y' => $player->y,
          'z' => $player->z
        ),
        'client_id' => $player->getClientId(),
        'health' => $player->getHealth(),
        'health_max' => $player->getMaxHealth(),
        'display_name' => $player->getDisplayName(),
        'display_tag' => $player->getNameTag(),
        'player_is_on_fire' => $player->isOnFire(),
        'player_is_sneaking' => $player->isSneaking(),
        'player_is_sprinting' => $player->isSprinting(),
        'operator' => $player->isOp()
      );
    }
  }

  //// Server

  public static function return_body_error($request, $code, $msg){
    $result = array(
      'code' => -1,
      'message' => "NotAllowed - ".$msg,
      'path' => $request->uri,
      'result' => array()
    );
    $response = new HTTPResponse($code, json_encode($result),null,null);
    $response->headers['Content-Type'] = 'application/json; charset=utf-8';
    return $response;
  }

  public static function return_body_json($request, $json_array){
    $result = array(
      'code' => 0,
      'message' => "success",
      'path' => $request->uri,
      'result' => $json_array
    );
    $response = new HTTPResponse(self::HTTP_OK, json_encode($result),null,null);
    $response->headers['Content-Type'] = 'application/json; charset=utf-8';
    return $response;
  }

  const HTTP_CONTINUE = 100;
  const HTTP_SWITCHING_PROTOCOLS = 101;
  const HTTP_OK = 200;
  const HTTP_CREATED = 201;
  const HTTP_ACCEPTED = 202;
  const HTTP_NONAUTHORITATIVE_INFORMATION = 203;
  const HTTP_NO_CONTENT = 204;
  const HTTP_RESET_CONTENT = 205;
  const HTTP_PARTIAL_CONTENT = 206;
  const HTTP_MULTIPLE_CHOICES = 300;
  const HTTP_MOVED_PERMANENTLY = 301;
  const HTTP_FOUND = 302;
  const HTTP_SEE_OTHER = 303;
  const HTTP_NOT_MODIFIED = 304;
  const HTTP_USE_PROXY = 305;
  const HTTP_UNUSED= 306;
  const HTTP_TEMPORARY_REDIRECT = 307;
  const HTTP_BAD_REQUEST = 400;
  const HTTP_UNAUTHORIZED  = 401;
  const HTTP_PAYMENT_REQUIRED = 402;
  const HTTP_FORBIDDEN = 403;
  const HTTP_NOT_FOUND = 404;
  const HTTP_METHOD_NOT_ALLOWED = 405;
  const HTTP_NOT_ACCEPTABLE = 406;
  const HTTP_PROXY_AUTHENTICATION_REQUIRED = 407;
  const HTTP_REQUEST_TIMEOUT = 408;
  const HTTP_CONFLICT = 409;
  const HTTP_GONE = 410;
  const HTTP_LENGTH_REQUIRED = 411;
  const HTTP_PRECONDITION_FAILED = 412;
  const HTTP_REQUEST_ENTITY_TOO_LARGE = 413;
  const HTTP_REQUEST_URI_TOO_LONG = 414;
  const HTTP_UNSUPPORTED_MEDIA_TYPE = 415;
  const HTTP_REQUESTED_RANGE_NOT_SATISFIABLE = 416;
  const HTTP_EXPECTATION_FAILED = 417;
  const HTTP_INTERNAL_SERVER_ERROR = 500;
  const HTTP_NOT_IMPLEMENTED = 501;
  const HTTP_BAD_GATEWAY = 502;
  const HTTP_SERVICE_UNAVAILABLE = 503;
  const HTTP_GATEWAY_TIMEOUT = 504;
  const HTTP_VERSION_NOT_SUPPORTED = 505;

  const HTTP_GET = 'GET';
  const HTTP_POST = 'POST';
}

?>
