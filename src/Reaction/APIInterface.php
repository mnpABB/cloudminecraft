<?php
namespace Reaction;

interface APIInterface{
  public function get($main, $request);

  public function post($main, $request);
}
